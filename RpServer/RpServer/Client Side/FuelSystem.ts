﻿/// <reference path="../types-gt-mp/index.d.ts" />

var LocalPlayer: GrandTheftMultiplayer.Client.Util.LocalHandle = null;
var IsSitVeh: GrandTheftMultiplayer.Client.Util.LocalHandle = null;
var res = API.getScreenResolutionMaintainRatio();
var Fuel = 0;

var timer = API.getGameTime();
API.onUpdate.connect(() => {
    if (API.getGameTime() - timer > 100) {
        if (IsSitVeh === null) return;
        var seat = API.getPlayerVehicleSeat(LocalPlayer);
        if (API.getVehicleEngineStatus(IsSitVeh) === true && seat == -1) {
            var rpm = API.getVehicleRPM(IsSitVeh);
            Fuel = Fuel - (rpm / 1000);
            API.drawText("Fuel " + ~~Fuel, 450, res.Height - 130, 1, 255, 255, 255, 255, 4, 2, true, true, 0);
            if (Fuel <= 0) API.setVehicleEngineStatus(IsSitVeh, false);
        }
    }
});

API.onPlayerEnterVehicle.connect((veh) => {
    LocalPlayer = API.getLocalPlayer();
    IsSitVeh = veh;
    Fuel = API.getEntitySyncedData(IsSitVeh, "Fuel");
});

API.onPlayerExitVehicle.connect((veh) => {
    if (IsSitVeh != null)
    {
        API.setEntitySyncedData(IsSitVeh, "Fuel", Fuel);
        IsSitVeh = null;
    }
});