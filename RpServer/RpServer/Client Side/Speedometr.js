/// <reference path="../types-gt-mp/index.d.ts" />
var res = API.getScreenResolutionMaintainRatio();
var timer = API.getGameTime();
API.onUpdate.connect(function () {
    if (API.getGameTime() - timer > 100) {
        var player = API.getLocalPlayer();
        var veh = API.getPlayerVehicle(player);
        if (API.getPlayerVehicleSeat(player) === -1 && API.getVehicleEngineStatus(veh) === true) {
            var speed = API.returnNative("GET_ENTITY_SPEED", 7, API.getPlayerVehicle(player));
            var SpeedMessage = Math.round(speed * 3.6);
            API.drawText(SpeedMessage + "km/h", 450, res.Height - 117, 1, 255, 255, 255, 255, 4, 2, true, true, 0);
        }
    }
});
//# sourceMappingURL=Speedometr.js.map