/// <reference path="../types-gt-mp/index.d.ts" />
var newCamera = null;
API.onServerEventTrigger.connect(function (eventName, args) {
    if (eventName === "CameraCreate") {
        newCamera = API.createCamera(args[0], args[1]);
        API.setActiveCamera(newCamera);
    }
    else if (eventName === "CameraPos") {
        API.setCameraPosition(newCamera, args[0]);
        API.setCameraRotation(newCamera, args[1]);
    }
    else if (eventName === "CameraDestroy") {
        newCamera = null;
        API.setGameplayCameraActive();
    }
});
//# sourceMappingURL=Camera.js.map