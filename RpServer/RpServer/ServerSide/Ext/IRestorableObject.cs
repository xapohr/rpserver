﻿namespace RpServer.ServerSide.Ext
{
    public interface IRestorableObject
    {
        void InitAfterRestore(params object[] args);
    }
}