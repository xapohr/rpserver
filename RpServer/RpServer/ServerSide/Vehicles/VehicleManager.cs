﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using System;
using System.Collections.Generic;

namespace RpServer.ServerSide.Vehicles
{
    public sealed class VehicleManager : Script
    {
        public static Dictionary<NetHandle, NetHandle> SeatVeh { get; set; } = new Dictionary<NetHandle, NetHandle>();
        public DateTime LastAnnounce;

        public VehicleManager()
        {
            API.onPlayerEnterVehicle += OnPlayerEnterVehicle;
            API.onPlayerExitVehicle += OnPlayerExitVehicleHandler;
            API.onUpdate += OnUpdateHandler;
        }

        private void OnUpdateHandler()
        {
            if (DateTime.Now.Subtract(LastAnnounce).TotalSeconds >= 30)
            {
                foreach (var veh in API.getAllVehicles())
                {
                    var ex = SeatVeh.ContainsKey(veh);
                    if(!ex)
                    {
                        if(API.getVehicleEngineStatus(veh))
                        {
                            var fuel = API.getEntitySyncedData(veh, "Fuel");
                            if (fuel <= 0) API.setVehicleEngineStatus(veh, false);
                            else API.setEntitySyncedData(veh, "Fuel", fuel - 0.33);
                            LastAnnounce = DateTime.Now;
                        }
                    }
                }
            }
        }

        private void OnPlayerEnterVehicle(Client player, NetHandle vehicle)
        {
            if (API.getPlayerVehicleSeat(player) == -1)
            {
                SeatVeh.Add(vehicle, vehicle);
            }
        }

        private void OnPlayerExitVehicleHandler(Client player, NetHandle vehicle)
        {
            var ex = SeatVeh.ContainsKey(vehicle);
            if (ex)
            {
                SeatVeh.Remove(vehicle);
            }
        }
    }
}
