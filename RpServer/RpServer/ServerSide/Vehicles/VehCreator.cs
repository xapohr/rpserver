﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;

namespace RpServer.ServerSide.Vehicles
{
    public sealed class VehCreator : Script
    {
        public VehCreator()
        {
            /*Здесь создаем машины
            Пример:
            new Vehicles(VehicleHash.T20, new Vector3(0, 0, 0), new Vector3(0, 0, 0), 0, 1);
            */
            API.onResourceStart += OnResourceStartHandler;
        }

        public void OnResourceStartHandler()
        {
            var veh = new Vehicles(VehicleHash.T20, new Vector3(-1023.62, -2711.57, 13.86), new Vector3(0, 0, 0), 1, 1);
        }
    }
}
