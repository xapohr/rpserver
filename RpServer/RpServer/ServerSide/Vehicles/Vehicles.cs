﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared;
using GrandTheftMultiplayer.Shared.Math;

namespace RpServer.ServerSide.Vehicles
{
    public sealed class Vehicles
    {
        public Vehicle Veh { get; private set; }

        public Vehicles(VehicleHash model, Vector3 pos, Vector3 rot, int color1, int color2)
        {
            Veh = API.shared.createVehicle(model, pos, rot, color1, color2);
            Veh.engineStatus = false;
            API.shared.setEntitySyncedData(Veh.handle, "Fuel", 100F);
        }
    }
}
