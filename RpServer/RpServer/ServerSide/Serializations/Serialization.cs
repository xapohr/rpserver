﻿using static RpServer.ServerSide.Serializations.SerializationHelper;
using System.Collections.Generic;
using System.Threading;
using RpServer.ServerSide.Account;

namespace RpServer.ServerSide.Serializations
{
    public static class Serialization
    {
        private const string Path = "resources\\RpServer\\data";
        private const string Name = "Accounts";
        private const string Ext = "mem";

        public static Dictionary<string, Player> AccountDictionary { get; private set; } = new Dictionary<string, Player>();//key - PlayerName
        public static ReaderWriterLockSlim Locker { get; } = new ReaderWriterLockSlim();

        public static void Restore()
        {
            var temp = RestoreObjectFromFile<Dictionary<string, Player>>(Path, Name, Ext);
            if (temp == null) return;
            AccountDictionary = temp;
        }

        public static void Save() => SaveObjectToFile(AccountDictionary, Path, Name, Ext);

    }
}