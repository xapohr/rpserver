﻿using static RpServer.ServerSide.Serializations.SerializationHelper;
using System.Collections.Generic;
using System.Threading;
using RpServer.ServerSide.Account;
using RpServer.ServerSide.Admins;

namespace RpServer.ServerSide.Serializations
{
    public static class SerializationAdmin
    {
        private const string Path = "resources\\RpServer\\data";
        private const string Name = "Admins";
        private const string Ext = "mem";

        public static Dictionary<string, Admin> AdminDictionary { get; private set; } = new Dictionary<string, Admin>();//key - PlayerName
        public static ReaderWriterLockSlim Locker { get; } = new ReaderWriterLockSlim();

        public static void Restore()
        {
            var temp = RestoreObjectFromFile<Dictionary<string, Admin>>(Path, Name, Ext);
            if (temp == null) return;
            AdminDictionary = temp;
        }

        public static void Save() => SaveObjectToFile(AdminDictionary, Path, Name, Ext);

    }
}