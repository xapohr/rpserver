﻿using GrandTheftMultiplayer.Server.API;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace RpServer.ServerSide.Serializations
{
    public static class SerializationHelper
    {
        public static void SaveObjectToFile<T>(T serializableObject, string pathToSave, string fileName, string fileExpansion)
        {
            var formatter = new BinaryFormatter();
            using (var fs = new FileStream($"{Directory.GetCurrentDirectory()}\\{pathToSave}\\{fileName}.{fileExpansion}", FileMode.OpenOrCreate))
                formatter.Serialize(fs, serializableObject);
        }
        public static T RestoreObjectFromFile<T>(string pathToFile, string fileName, string fileExpansion)
        {
            var formatter = new BinaryFormatter();
            T restoredObject;
            if (!File.Exists($"{Directory.GetCurrentDirectory()}\\{pathToFile}\\{fileName}.{fileExpansion}"))
            {
                API.shared.consoleOutput($"Ошибка при попытке восстановления {fileName}. Файл не найден");
                return default(T);
            }
            using (var fs = new FileStream($"{Directory.GetCurrentDirectory()}\\{pathToFile}\\{fileName}.{fileExpansion}", FileMode.Open))
                restoredObject = (T)formatter.Deserialize(fs);
            return restoredObject;
        }
    }
}