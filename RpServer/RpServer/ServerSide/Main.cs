﻿using GrandTheftMultiplayer.Server.API;
using RpServer.ServerSide.Serializations;

namespace RpServer.ServerSide
{
    public class Main : Script
    {
        public Main()
        {
            API.onResourceStart += OnResourceStartHandler;
            API.onResourceStop += OnResourceStopHandler;
        }

        private void OnResourceStartHandler()
        {
            Serialization.Restore();
            Serialization.Locker.EnterReadLock();
            foreach (var account in Serialization.AccountDictionary.Values) account.IsOnline = false;
            Serialization.Locker.ExitReadLock();

            SerializationAdmin.Restore();
            SerializationAdmin.Locker.EnterReadLock();
            foreach (var Admin in SerializationAdmin.AdminDictionary.Values) Admin.IsOnline = false;
            SerializationAdmin.Locker.ExitReadLock();
        }

        private void OnResourceStopHandler()
        {
            Serialization.Save();
            SerializationAdmin.Save();
        }
    }
}
