﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared.Math;
using RpServer.ServerSide.Account;
using RpServer.ServerSide.Serializations;

namespace RpServer.ServerSide.Global
{
    public sealed class ConnectPlayer : Script
    {
        private Vector3 CamPos = new Vector3(1810f, 0f, 220f);
        private Vector3 CamRot = new Vector3(-10f, 0f, 98f);

        public ConnectPlayer()
        {
            API.onPlayerFinishedDownload += OnPlayerFinishedDownloadHandler;
            API.onPlayerDisconnected += OnPlayerDisconnectedHandler;
        }

        private void OnPlayerFinishedDownloadHandler(Client player)
        {
            player.triggerEvent("CameraCreate", CamPos, CamRot);
            API.setEntityPosition(player, new Vector3(1800f, 20f, 200f));
            API.setEntityDimension(player, -1);
            API.setEntityPositionFrozen(player, true);
            Serialization.Locker.EnterReadLock();
            var isAccountExits = Serialization.AccountDictionary.ContainsKey(player.name);
            Serialization.Locker.ExitReadLock();
            if (isAccountExits)
            {
                API.sendChatMessageToPlayer(player, "Чтобы авторизоваться введите ~y~/login [Пароль]");
            }
            else
            {
                API.sendChatMessageToPlayer(player, "Чтобы зарегистрироваться введите ~y~/register [Пароль]");
            }
        }

        private void OnPlayerDisconnectedHandler(Client player, string reason)
        {
            Serialization.Locker.EnterWriteLock();
            var account = Serialization.AccountDictionary[player.name];
            Serialization.Locker.ExitWriteLock();
            account.IsOnline = false;
            PlayerManager.PlayerId.Remove(account.id);

            SerializationAdmin.Locker.EnterReadLock();
            var isAdminExits = SerializationAdmin.AdminDictionary.ContainsKey(player.name);
            if (isAdminExits)
            {
                var admin = SerializationAdmin.AdminDictionary[player.name];
                SerializationAdmin.Locker.ExitReadLock();
                admin.IsOnline = false;
            }
        }

        [Command("register")]
        public void Register(Client player, string password)
        {
            var newAcc = Player.New(player, password);
            Serialization.Locker.EnterWriteLock();
            Serialization.AccountDictionary.Add(newAcc.PlayerName, newAcc);
            Serialization.Locker.ExitWriteLock();
            newAcc.AuthPlayer();
        }

        [Command("login")]
        public void Login(Client player, string password)
        {
            Serialization.Locker.EnterReadLock();
            var isAccountExits = Serialization.AccountDictionary.ContainsKey(player.name);
            if (isAccountExits)
            {
                var account = Serialization.AccountDictionary[player.name];
                Serialization.Locker.ExitReadLock();
                if (!account.PlayerPass.Equals(API.getHashSHA256(password)))
                {
                    API.sendChatMessageToPlayer(player, "Пароль не верный. Повторите вход заново.");
                    return;
                }
                if (account.IsOnline)
                {
                    API.sendChatMessageToPlayer(player, "Персонаж уже в игре. Повторите вход заново.");
                    return;
                }
                account.InitAfterRestore(player);
                account.AuthPlayer();
            }
        }
    }
}
