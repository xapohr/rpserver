﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RpServer.ServerSide.Global
{
    public sealed class PlayerCommands : Script 
    {
        [Command("engine")]
        public void Engine(Client player)
        {            
            if (API.getPlayerVehicleSeat(player) == -1)
            {
                var veh = API.getPlayerVehicle(player);
                API.setVehicleEngineStatus(veh, !API.getVehicleEngineStatus(veh));
            }
        }
    }
}
