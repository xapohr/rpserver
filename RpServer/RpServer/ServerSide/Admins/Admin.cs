﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using RpServer.ServerSide.Serializations;
using System;

namespace RpServer.ServerSide.Admins
{
    [Serializable]
    public sealed class Admin
    {
        public string PlayerName { get; private set; }
        public string AdminPass { get; private set; }
        public byte Level { get; private set; }
        public bool IsOnline { get; set; }
        public Vehicles.Vehicles Veh
        {
            get => _veh;
            set => _veh = value;
        }

        public static Admin New(string name, string pass, byte level) => new Admin
        {
            PlayerName = name,
            AdminPass = API.shared.getHashSHA256(pass),
            Level = level
        };

        public void AuthAdmin(Client client)
        {
            IsOnline = true;
            Veh = null;
            client.sendChatMessage("Вы авторизировались как админестратор");
        }

        public static Admin GetAdmin(string name)
        {
            SerializationAdmin.Locker.EnterReadLock();
            var isAdminExits = SerializationAdmin.AdminDictionary.ContainsKey(name);
            if (isAdminExits)
            {
                var admin = SerializationAdmin.AdminDictionary[name];
                SerializationAdmin.Locker.ExitReadLock();
                return admin;
            }
            return null;
        }

        [NonSerialized]
        private Vehicles.Vehicles _veh;
    }
}
