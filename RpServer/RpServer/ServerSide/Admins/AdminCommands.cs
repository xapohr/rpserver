﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Constant;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using GrandTheftMultiplayer.Shared;
using RpServer.ServerSide.Account;
using RpServer.ServerSide.Serializations;
using RpServer.ServerSide.Vehicles;

namespace RpServer.ServerSide.Admins
{
    public sealed class AdminCommands : Script
    {
        [Command("alogin")]
        public void ALogin(Client client, string pass)
        {
            SerializationAdmin.Locker.EnterReadLock();
            var isAdminExits = SerializationAdmin.AdminDictionary.ContainsKey(client.name);
            if (isAdminExits)
            {
                var admin = SerializationAdmin.AdminDictionary[client.name];
                SerializationAdmin.Locker.ExitReadLock();
                if (!admin.AdminPass.Equals(API.getHashSHA256(pass)))
                {
                    API.sendChatMessageToPlayer(client, "Пароль не верный!");
                    return;
                }
                if (admin.IsOnline)
                {
                    API.sendChatMessageToPlayer(client, "Вы уже ааторизованый");
                    return;
                }
                admin.AuthAdmin(client);                
            }
        }

        [Command("makeadmin")]
        public void MakeAdmin(Client client, string IdOrName, string pass, byte ALevel)
        {
            var player = PlayerManager.GetPlayer(client, IdOrName);
            if (player == null || ALevel > 6 || ALevel < 0) return;
            var admin = Admin.New(player.PlayerName, pass, ALevel);
            SerializationAdmin.Locker.EnterWriteLock();
            SerializationAdmin.AdminDictionary.Add(player.PlayerName, admin);
            SerializationAdmin.Locker.ExitWriteLock();
            admin.IsOnline = true;
        }

        [Command("veh")]
        public void Veh(Client client, VehicleHash model, int color1, int color2)
        {
            var admin = Admin.GetAdmin(client.name);
            if (admin == null) return;
            var poz = client.position;
            var rot = client.rotation;
            poz.X += 2;
            if (admin.Veh == null)
            {
                admin.Veh = new Vehicles.Vehicles(model, poz, rot, color1, color2);
            }
            else
            {
                admin.Veh.Veh.delete();
                admin.Veh = new Vehicles.Vehicles(model, poz, rot, color1, color2);
            }
        }

        [Command("admins")]
        public void Admins(Client client)
        {
            SerializationAdmin.Locker.EnterReadLock();
            foreach (var Admin in SerializationAdmin.AdminDictionary.Values)
            {
                client.sendChatMessage("Админестратор: " + Admin.PlayerName + " уровень: " + Admin.Level + " онлайн " + Admin.IsOnline);
            }
            SerializationAdmin.Locker.ExitReadLock();
        }
    }
}
