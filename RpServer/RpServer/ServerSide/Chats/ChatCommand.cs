﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Server.Managers;
using RpServer.ServerSide.Serializations;

namespace RpServer.ServerSide.Chats
{
    class ChatCommand : Script
    {
        [Command("me")]
        public void Me(Client player, string message)
        {
            foreach (Client players in API.getPlayersInRadiusOfPlayer(5F, player))
            {
                player.sendChatMessage("~p~" + player.name + " " + message);
            }
        }

        [Command("s")]
        public void S(Client player, string message)
        {
            Serialization.Locker.EnterWriteLock();
            var account = Serialization.AccountDictionary[player.name];
            Serialization.Locker.ExitWriteLock();
            foreach (Client players in API.getPlayersInRadiusOfPlayer(10F, player))
            {
                player.sendChatMessage(player.name + "(" + account.id + ")" + " кричит", message);
            }
        }

        [Command("w")]
        public void W(Client player, string message)
        {
            Serialization.Locker.EnterWriteLock();
            var account = Serialization.AccountDictionary[player.name];
            Serialization.Locker.ExitWriteLock();
            foreach (Client players in API.getPlayersInRadiusOfPlayer(1F, player))
            {
                player.sendChatMessage(player.name + "(" + account.id + ")" + " шепчет", message);
            }
        }
    }
}
