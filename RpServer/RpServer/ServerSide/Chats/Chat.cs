﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using RpServer.ServerSide.Serializations;

namespace RpServer.ServerSide.Chats
{
    public sealed class Chat : Script
    {
        public Chat()
        {
            API.onChatMessage += OnChatMessageHandler;
        }
        public void OnChatMessageHandler(Client player, string message, CancelEventArgs e)
        {
            Serialization.Locker.EnterWriteLock();
            var account = Serialization.AccountDictionary[player.name];
            Serialization.Locker.ExitWriteLock();
            if(account.IsOnline)
            {
                e.Cancel = true;
                foreach (Client players in API.getPlayersInRadiusOfPlayer(5F, player))
                {
                    players.sendChatMessage(player.name + "(" + account.id + ")", message);
                }
            }
            else
            {
                player.sendChatMessage("~r~[Ошибка]Вы не авторизированны!");
            }
        }
    }
}
