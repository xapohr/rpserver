﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using GrandTheftMultiplayer.Shared.Math;
using RpServer.ServerSide.Ext;
using System;

namespace RpServer.ServerSide.Account
{
    [Serializable]
    public sealed class Player : IRestorableObject
    {
        public string PlayerName { get; private set; }
        public string PlayerPass { get; private set; }
        public DateTime RegisterDate { get; private set; }
        public DateTime LastLoginDate { get; private set; }
        public bool IsOnline { get; set; }
        public long Money { get; private set; }
        public long BankMoney { get; private set; }
        public Client Client
        {
            get => _client;
            private set => _client = value;
        }
        public int id
        {
            get => _id;
            set => _id = value;
        }

        public static Player New(Client player, string pass) => new Player
        {
            PlayerName = player.name,
            PlayerPass = API.shared.getHashSHA256(pass),
            RegisterDate = DateTime.Now,
            Client = player,
        };

        public void AuthPlayer()
        {
            id = PlayerManager.SetIdPlayer(Client, this);
            IsOnline = true;
            LastLoginDate = DateTime.Now;
            Client.triggerEvent("CameraDestroy");
            Client.freezePosition = false;
            Client.dimension = 0;
            Client.position = new Vector3(-1037.62, -2737.57, 20.16);
            Client.rotation = new Vector3(0, 0, 0);
            API.shared.sendChatMessageToPlayer(Client, "Поздравляем. Вы успешно авторизовались.");
        }

        public void InitAfterRestore(params object[] args)
        {
            var client = args[0] as Client;
            if (client == null) return;
            Client = client;
        }
        
        public void AddMoney(int count)
        {
            lock(_moneyLocker)
            {
                Money += count;
            }
        }

        public void RemoveMoney(int count)
        {
            lock(_moneyLocker)
            {
                Money -= count;
            }
        }

        [NonSerialized]
        private Client _client;
        [NonSerialized]
        private int _id;
        [NonSerialized]
        private object _moneyLocker = new object();
    }
}
