﻿using GrandTheftMultiplayer.Server.API;
using GrandTheftMultiplayer.Server.Elements;
using RpServer.ServerSide.Serializations;
using System.Collections.Generic;

namespace RpServer.ServerSide.Account
{
    public sealed class PlayerManager : Script
    {
        public static Dictionary<int, Player> PlayerId { get; set; } = new Dictionary<int, Player>();

        public static int SetIdPlayer(Client client, Player player)
        {
            List<Client> players = API.shared.getAllPlayers();
            int id = players.IndexOf(client);
            PlayerId.Add(id, player);
            return id;
        }

        public static Player GetPlayer(Client sender, string IdOrName)
        {
            int id;
            if (int.TryParse(IdOrName, out id))
            {
                if (PlayerId[id] == null)
                {
                    API.shared.sendChatMessageToPlayer(sender, "Игрока нет на сервере!");
                    return null;
                }
                Player player = PlayerId[id];
                return player;
            }
            else
            {
                Serialization.Locker.EnterReadLock();
                var isAccountExits = Serialization.AccountDictionary.ContainsKey(IdOrName);
                Serialization.Locker.ExitReadLock();
                if (isAccountExits)
                {
                    Serialization.Locker.EnterReadLock();
                    Player player = Serialization.AccountDictionary[IdOrName];
                    Serialization.Locker.ExitReadLock();
                    return player;
                }
                else
                {
                    API.shared.sendChatMessageToPlayer(sender, "Игрока нет на сервере!");
                    return null;
                }
            }
        }
    }
}
